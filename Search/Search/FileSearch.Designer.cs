﻿namespace Search
{
    partial class FileSearcher
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.TbxDir = new System.Windows.Forms.TextBox();
            this.BtnStart = new System.Windows.Forms.Button();
            this.TbxPattern = new System.Windows.Forms.TextBox();
            this.lblDir = new System.Windows.Forms.Label();
            this.lblNameTemplate = new System.Windows.Forms.Label();
            this.panel = new System.Windows.Forms.Panel();
            this.BtnStop = new System.Windows.Forms.Button();
            this.lblText = new System.Windows.Forms.Label();
            this.TbxText = new System.Windows.Forms.TextBox();
            this.lblOutput = new System.Windows.Forms.Label();
            this.trvFounded = new System.Windows.Forms.TreeView();
            this.lblProcessFile = new System.Windows.Forms.Label();
            this.lblTimerName = new System.Windows.Forms.Label();
            this.lblProcFilesCountName = new System.Windows.Forms.Label();
            this.lblProcFilesCount = new System.Windows.Forms.Label();
            this.lblTimer = new System.Windows.Forms.Label();
            this.panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // TbxDir
            // 
            this.TbxDir.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TbxDir.Location = new System.Drawing.Point(123, 16);
            this.TbxDir.Name = "TbxDir";
            this.TbxDir.Size = new System.Drawing.Size(338, 26);
            this.TbxDir.TabIndex = 0;
            this.TbxDir.TextChanged += new System.EventHandler(this.TbxDir_TextChanged);
            // 
            // BtnStart
            // 
            this.BtnStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.BtnStart.Location = new System.Drawing.Point(498, 35);
            this.BtnStart.Name = "BtnStart";
            this.BtnStart.Size = new System.Drawing.Size(111, 30);
            this.BtnStart.TabIndex = 3;
            this.BtnStart.Text = "Старт";
            this.BtnStart.UseVisualStyleBackColor = true;
            this.BtnStart.Click += new System.EventHandler(this.BtnStart_Click);
            // 
            // TbxPattern
            // 
            this.TbxPattern.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TbxPattern.Location = new System.Drawing.Point(123, 61);
            this.TbxPattern.Name = "TbxPattern";
            this.TbxPattern.Size = new System.Drawing.Size(338, 26);
            this.TbxPattern.TabIndex = 5;
            this.TbxPattern.TextChanged += new System.EventHandler(this.TbxPattern_TextChanged);
            // 
            // lblDir
            // 
            this.lblDir.AutoSize = true;
            this.lblDir.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblDir.Location = new System.Drawing.Point(24, 21);
            this.lblDir.Name = "lblDir";
            this.lblDir.Size = new System.Drawing.Size(93, 17);
            this.lblDir.TabIndex = 6;
            this.lblDir.Text = "Директория:";
            // 
            // lblNameTemplate
            // 
            this.lblNameTemplate.AutoSize = true;
            this.lblNameTemplate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblNameTemplate.Location = new System.Drawing.Point(9, 67);
            this.lblNameTemplate.Name = "lblNameTemplate";
            this.lblNameTemplate.Size = new System.Drawing.Size(108, 17);
            this.lblNameTemplate.TabIndex = 7;
            this.lblNameTemplate.Text = "Шаблон имени:";
            // 
            // panel
            // 
            this.panel.BackColor = System.Drawing.SystemColors.Control;
            this.panel.Controls.Add(this.BtnStop);
            this.panel.Controls.Add(this.lblText);
            this.panel.Controls.Add(this.TbxText);
            this.panel.Controls.Add(this.TbxDir);
            this.panel.Controls.Add(this.lblNameTemplate);
            this.panel.Controls.Add(this.BtnStart);
            this.panel.Controls.Add(this.lblDir);
            this.panel.Controls.Add(this.TbxPattern);
            this.panel.Location = new System.Drawing.Point(12, 37);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(641, 156);
            this.panel.TabIndex = 9;
            // 
            // BtnStop
            // 
            this.BtnStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.BtnStop.Location = new System.Drawing.Point(498, 81);
            this.BtnStop.Name = "BtnStop";
            this.BtnStop.Size = new System.Drawing.Size(111, 30);
            this.BtnStop.TabIndex = 10;
            this.BtnStop.Text = "Стоп";
            this.BtnStop.UseVisualStyleBackColor = true;
            this.BtnStop.Click += new System.EventHandler(this.BtnStop_Click);
            // 
            // lblText
            // 
            this.lblText.AutoSize = true;
            this.lblText.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblText.Location = new System.Drawing.Point(67, 105);
            this.lblText.Name = "lblText";
            this.lblText.Size = new System.Drawing.Size(50, 17);
            this.lblText.TabIndex = 9;
            this.lblText.Text = "Текст:";
            // 
            // TbxText
            // 
            this.TbxText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TbxText.Location = new System.Drawing.Point(123, 99);
            this.TbxText.Name = "TbxText";
            this.TbxText.Size = new System.Drawing.Size(338, 26);
            this.TbxText.TabIndex = 8;
            // 
            // lblOutput
            // 
            this.lblOutput.AutoSize = true;
            this.lblOutput.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblOutput.Location = new System.Drawing.Point(669, 9);
            this.lblOutput.Name = "lblOutput";
            this.lblOutput.Size = new System.Drawing.Size(140, 17);
            this.lblOutput.TabIndex = 8;
            this.lblOutput.Text = "Результаты поиска:";
            // 
            // trvFounded
            // 
            this.trvFounded.Location = new System.Drawing.Point(672, 37);
            this.trvFounded.Name = "trvFounded";
            this.trvFounded.Size = new System.Drawing.Size(383, 295);
            this.trvFounded.TabIndex = 10;
            // 
            // lblProcessFile
            // 
            this.lblProcessFile.AutoSize = true;
            this.lblProcessFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblProcessFile.Location = new System.Drawing.Point(21, 214);
            this.lblProcessFile.Name = "lblProcessFile";
            this.lblProcessFile.Size = new System.Drawing.Size(0, 17);
            this.lblProcessFile.TabIndex = 10;
            // 
            // lblTimerName
            // 
            this.lblTimerName.AutoSize = true;
            this.lblTimerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblTimerName.Location = new System.Drawing.Point(87, 291);
            this.lblTimerName.Name = "lblTimerName";
            this.lblTimerName.Size = new System.Drawing.Size(58, 17);
            this.lblTimerName.TabIndex = 11;
            this.lblTimerName.Text = "Время: ";
            // 
            // lblProcFilesCountName
            // 
            this.lblProcFilesCountName.AutoSize = true;
            this.lblProcFilesCountName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblProcFilesCountName.Location = new System.Drawing.Point(82, 257);
            this.lblProcFilesCountName.Name = "lblProcFilesCountName";
            this.lblProcFilesCountName.Size = new System.Drawing.Size(63, 17);
            this.lblProcFilesCountName.TabIndex = 12;
            this.lblProcFilesCountName.Text = "Файлы: ";
            // 
            // lblProcFilesCount
            // 
            this.lblProcFilesCount.AutoSize = true;
            this.lblProcFilesCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblProcFilesCount.Location = new System.Drawing.Point(148, 257);
            this.lblProcFilesCount.Name = "lblProcFilesCount";
            this.lblProcFilesCount.Size = new System.Drawing.Size(0, 17);
            this.lblProcFilesCount.TabIndex = 14;
            // 
            // lblTimer
            // 
            this.lblTimer.AutoSize = true;
            this.lblTimer.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblTimer.Location = new System.Drawing.Point(148, 291);
            this.lblTimer.Name = "lblTimer";
            this.lblTimer.Size = new System.Drawing.Size(0, 17);
            this.lblTimer.TabIndex = 13;
            // 
            // FileSearcher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1075, 354);
            this.Controls.Add(this.lblProcFilesCount);
            this.Controls.Add(this.lblTimer);
            this.Controls.Add(this.lblProcFilesCountName);
            this.Controls.Add(this.lblTimerName);
            this.Controls.Add(this.lblProcessFile);
            this.Controls.Add(this.trvFounded);
            this.Controls.Add(this.lblOutput);
            this.Controls.Add(this.panel);
            this.Name = "FileSearcher";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Поиск файлов";
            this.Load += new System.EventHandler(this.FileSearcher_Load);
            this.panel.ResumeLayout(false);
            this.panel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TbxDir;
        private System.Windows.Forms.Button BtnStart;
        private System.Windows.Forms.TextBox TbxPattern;
        private System.Windows.Forms.Label lblDir;
        private System.Windows.Forms.Label lblNameTemplate;
        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.Label lblOutput;
        private System.Windows.Forms.TreeView trvFounded;
        private System.Windows.Forms.Label lblText;
        private System.Windows.Forms.TextBox TbxText;
        private System.Windows.Forms.Label lblProcessFile;
        private System.Windows.Forms.Label lblTimerName;
        private System.Windows.Forms.Button BtnStop;
        private System.Windows.Forms.Label lblProcFilesCountName;
        private System.Windows.Forms.Label lblProcFilesCount;
        private System.Windows.Forms.Label lblTimer;
    }
}

