﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;

namespace Search
{
    public partial class FileSearcher : Form
    {
        List<string> Files = new List<string>();
        string NamePattern;
        string SearchText;
        
        EventWaitHandle WaitHandler;
        ThreadCond CondExe = ThreadCond.Stop;
        DateTime StartTime;
        DateTime PauseTimeStart;
        TimeSpan PauseTimeSpan =new TimeSpan(0);

        int ProcFilesCount = 0;

        public FileSearcher()
        {
            InitializeComponent();
            WaitHandler = new EventWaitHandle(false, EventResetMode.AutoReset);
        }

        private void FileSearcher_Load(object sender, EventArgs e)
        {
            Init();
        }

        private void Init()
        {
            TbxDir.Text = ConfigurationManager.AppSettings["Dir"];
            NamePattern = ConfigurationManager.AppSettings["NamePattern"];
            TbxPattern.Text = NamePattern;
            SearchText = ConfigurationManager.AppSettings["SearchText"];
            TbxText.Text = SearchText;
        }

        private void BtnStart_Click(object sender, EventArgs e)
        {
            if (CondExe == ThreadCond.Exe)
            {
                PauseTimeStart = DateTime.Now;
                CondExe = ThreadCond.Pause;
                BtnStart.Text = "Продолжить";
            }
            else
            {
                BtnStart.Text = "Пауза";
                if (CondExe == ThreadCond.Stop)
                {
                    StartTime = DateTime.Now;
                    CondExe = ThreadCond.Exe;
                    SearchEngine();
                }
                else
                {
                    PauseTimeSpan +=( DateTime.Now - PauseTimeStart);
                    CondExe = ThreadCond.Exe;
                    WaitHandler.Set();
                }
            }
        }

        private void SearchEngine()
        {
            string rootName = TbxDir.Text;
            TreeNode rootNode = new TreeNode(rootName);
            Dir root = new Dir(rootName, rootNode); ;

            trvFounded.Nodes.Clear();
            trvFounded.Nodes.Add(rootNode);
            root = new Dir(rootName, rootNode);
            ThreadPool.QueueUserWorkItem(new WaitCallback((s) =>
            {
                DirSearch(root);

                BeginInvoke(new MethodInvoker(delegate
                {
                    Stop();
                }));
            }));

        }

        private void DirSearch(Dir localRoot)
        {
            string[] dirNames = Directory.GetDirectories(localRoot.Name);
            foreach (string dirName in dirNames)
            {
                TreeNode dirNode = new TreeNode(Path.GetFileName(dirName));
                BeginInvoke(new MethodInvoker(delegate
                {
                    localRoot.AddNode(dirNode);
                }));
                Dir currentDir = new Dir(dirName, dirNode);
                DirSearch(currentDir);
                CheckFiles(currentDir);
               
            }
        }

        private void CheckFiles(Dir localRoot)
        {
            string[] files = Directory.GetFiles(localRoot.Name);
            foreach (string file in files)
            {
                CheckFileName(file, localRoot);
            }
        }

        private void CheckFileName(string file, Dir localRoot)
        {
            if (Regex.IsMatch(file, NamePattern, RegexOptions.IgnoreCase))
                CheckText( file,  localRoot);
        }

        private void CheckText(string file, Dir localRoot)
        {
            if (CondExe == ThreadCond.Stop)
            {
                BeginInvoke(new MethodInvoker(delegate
                {
                    trvFounded.Nodes.Clear();
                }));
                return;
            }
            if (CondExe == ThreadCond.Pause)
            {
                WaitHandler.WaitOne();
            }

            string fileText = File.ReadAllText(file);
            BeginInvoke(new MethodInvoker(delegate
            {
                localRoot.AddFile(file);
                lblProcessFile.Text = file;
                lblProcFilesCount.Text =ProcFilesCount.ToString();
                lblTimer.Text = (DateTime.Now - StartTime - PauseTimeSpan).ToString(@"hh\:mm\:ss");
                ProcFilesCount++;

                this.Refresh();
            }));
            Thread.Sleep(200);
        }

        private void TbxDir_TextChanged(object sender, EventArgs e)
        {
            ConfigurationManager.AppSettings["Dir"] = TbxDir.Text;
        }

        private void TbxPattern_TextChanged(object sender, EventArgs e)
        {
            ConfigurationManager.AppSettings["Pattern"] = NamePattern;
        }

        private void BtnStop_Click(object sender, EventArgs e)
        {
            Stop();
        }
        
        private void Stop()
        {
            BtnStart.Text = "Старт";
            CondExe = ThreadCond.Stop;
            ProcFilesCount = 0;
        }
    }

    struct Dir
    {
        public string Name;
        public TreeNode Node;

        public Dir(string name, TreeNode node)
        {
            this.Name = name;
            this.Node = node;
        }

        public void AddNode(TreeNode dirNode)
        {
            this.Node.Nodes.Add(dirNode);
            this.Node.Expand();
        }

        public void AddFile(string name)
        {
            TreeNode dirNode = new TreeNode(Path.GetFileName(name));
            AddNode(dirNode);
        }
    }

    enum ThreadCond
    {
        Exe,
        Pause,
        Stop
    }
}
